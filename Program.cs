﻿using System;
using System.Collections.Generic;

namespace week_4_exercise_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var movies = new string[5] {
                            "Cars",
                            "The Mummy",
                            "Angry Birds",
                            "Logan",
                            "Inception"};
            for(var i=0;i<movies.Length;i++)
            {
                Console.WriteLine(movies[i]);
            }
            Console.WriteLine();

            Console.WriteLine("Please enter a new movie title");
            movies[1] = Console.ReadLine();
            Console.WriteLine();
            
            Console.WriteLine("And another..");
            movies[3] = Console.ReadLine();
            Console.WriteLine();

            Array.Sort(movies);
            Console.WriteLine("Array sorted!");
            Console.WriteLine();

            Console.WriteLine(string.Join(", ",movies));
            Console.WriteLine();

            var restraunts = new List<string> {
                                    "McDonalds",
                                    "KFC",
                                    "Burger King",
                                    "Dominoes",
                                    "Wendies"};
            Console.WriteLine("Un-sorted entries:");
            Console.WriteLine(string.Join(", ",restraunts));
            Console.WriteLine();

            Console.WriteLine("Sorted entries:");
            restraunts.Sort();
            Console.WriteLine(string.Join(", ",restraunts));

            Console.ReadKey();
            Console.WriteLine($"Entry: \"{restraunts[3]}\" removed from the List.");
            restraunts.Remove(restraunts[3]);
            Console.WriteLine();

            Console.WriteLine("Remaining entries:");
            Console.WriteLine(string.Join(", ",restraunts));
        }
    }
}
